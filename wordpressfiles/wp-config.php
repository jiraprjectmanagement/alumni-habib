<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'alumni');

/** MySQL database username */
define('DB_USER', 'alumni');

/** MySQL database password */
define('DB_PASSWORD', 'aLUmn!!021++');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3>yCrHYM>XB)=}q,M =fVL:l$As:3}jo~ Z{rK_IHp>G[f]aV]_DiZ`.7JBAwujb');
define('SECURE_AUTH_KEY',  'n{(jP`%<E]?>lK_}l{E}]l<AKnj|IqLHb3_f;,Zi>xFh|uU#6{!qc8yb-LR#?rcw');
define('LOGGED_IN_KEY',    '?jGTac<T^@ofM!_ n#_>$ze2Xoly3REmH`1`_.7{gp5qT2W|L&c47.nGZl@WszH*');
define('NONCE_KEY',        '<:$sz(BByK9`JN.5!)Z5.!`%^=+`gy-zK.<H6#i7dYw;!og?D!{!TFVR)u:Y7q*;');
define('AUTH_SALT',        '0rNhN>nx/f`fu= MvEBrFrL^KcS9kbjTBplQb/b:h@>sa1e@@(oeQ0H3&gAE`2Xv');
define('SECURE_AUTH_SALT', 'KqURB]@4p|*Pmmjd,BqwG~^P.DYeD.>P4WY(]I>U,ETf+i?ilTl-[VC (kz<?lIv');
define('LOGGED_IN_SALT',   'Pme;ivuTZt#Lmuoj!:EMOwT*oU |D#w[NpuHSCth~s%-8g@W-u* 235+VuT+>zVX');
define('NONCE_SALT',       '<zq5kb2vPnjGZp+pZJt[5epl4ZZjqi3Vu.~y-&L$l=@<Y6+P1w!sE_g5/RH4pg2#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');