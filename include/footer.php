
<section class="contact-alumni-office" id="Contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="contact-content">
                    <h1 class="sec-heading"> <span>Contact</span> <br> Alumni Office</h1>
                    <ul class="contac-details">
                        <li>
                            <img src="img/loc.svg" width="20" height="20" alt="">
                            <p>
                                Office of Alumni Affairs, Habib University, Block 18, Gulistan-e-Jauhar – University Avenue, Off Shahrah-e-Faisal, Karachi – 75290, Sindh, Pakistan
                            </p>

                        </li>
                        <li>
                            <img src="img/em.svg" width="20" height="20" alt="">
                            <p>
                                alumni.affairs@habib.edu.pk
                            </p>
                        </li>
                        <li>
                            <img src="img/ph.svg" width="20" height="20" alt="">
                            <p>
                                +92 21 1110 42242 Ext. 4581
                            </p>
                        </li>
                        <li>
                            <img src="img/fb.svg" width="20" height="20" alt="">
                            <p>
                                facebook.com/groups/habibuniversityalumni
                            </p>
                        </li>
                        <li>
                            <img src="img/ti.svg" width="20" height="20" alt="">
                            9:00 am – 5:00 pm, Monday through Friday, 2nd Saturday of each month.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="google-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3618.7684150641808!2d67.13608751537706!3d24.905879849568592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb33906ead1899d%3A0x3c0681e6f7d5dc14!2sHabib%20University!5e0!3m2!1sen!2s!4v1644827999335!5m2!1sen!2s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>


<footer class="main-footer">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-5">
                <div class="foter-right">
                    <div class="deatail-foot-content">
                        <div class="footer-logo">
                            <a href="http://habib.edu.pk/" target="_blank">
                                <img src="img/HU-logo-footer.svg" alt="Habib University">
                            </a>
                        </div>
                        <h4 class="mt-3">Main Campus</h4>
                        <p>Office of Alumni Affairs, Habib University, Block 18, Gulistan-e-Jauhar – University Avenue, Off Shahrah-e-Faisal, Karachi – 75290, Sindh, Pakistan</p>
                        <p><a href="tel:+92 21 1110 42242">+92 21 1110 42242</a></p>
                        <h4 class="mt-3">Follow Habib University</h4>
                        <ul class="social-links">
                            <li> <a target="_blank" href="https://www.facebook.com/groups/habibuniversityalumni"><i class="fa-brands fa-facebook-f"></i></a> </li>
                            <li> <a target="_blank" href="https://www.linkedin.com/school/habib-university/"> <i class="fa-brands fa-linkedin-in"></i></a> </li>
                            <li> <a target="_blank" href="https://www.instagram.com/habibuniversity/"> <i class="fa-brands fa-instagram"></i></a> </li>
                            <li> <a target="_blank" href="https://twitter.com/habibuniversity"> <i class="fa-brands fa-twitter"></i></a> </li>
                            <li> <a target="_blank" href="https://www.youtube.com/user/HabibUni"> <i class="fa-brands fa-youtube"></i></a> </li>
                            <li> <a target="_blank" href="https://habib.edu.pk/"> <i class="fa-solid fa-globe"></i></a> </li>
                            
                        </ul>
                        <p class="copy-right">© Habib University - All Rights Reserved</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="footer-right">
                    <h4>Quick Links</h4> 
                    <div class="footer-nav">
                        <ul class="quick-links">
                            <li> <a href="index"> Home</a></li>
                            <li> <a href="volunteering"> Volunteering</a></li>
                            <li> <a href="https://habib.edu.pk/career-services/" target="_blank"> Careers</a></li>
                            <li>  Giving</li>
                            <li class="sub-li"><a href="https://giving.habib.edu.pk/" target="_blank">- Give Now</a></li>
                        </ul>
                        <ul class="quick-links">
                            <li><a href=""> Alumni Association</a></li>
                            <li><a href=""> Office of Alumni Affair</a></li>
                            <li> Alumni Portal</li>
                            <li class="sub-li"><a href="alumni-career-update">- Alumni career Update</a></li>
                            <li class="sub-li"><a href="https://habibuniversity.sharepoint.com/sites/Alumni" target="_blank">- Alumni Policies</a></li>
                            <li class="sub-li"><a href="https://habibuniversity.sharepoint.com/sites/alumni/Pages/jobs.aspx" target="_blank">- Alumni Jobs</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

    <script type="" src="js/plugin.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script type="" src="js/custom.js"></script>
    
</body>
</html>