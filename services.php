<?php include 'include/header.php' ?>

<!-- Space -->
<div class="inner-page-header-seprator"></div>
<!-- Space -->


<section class="alumni-service">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h1 class="sec-heading">
                    Alumni - Services
                </h1>
                <p> <b>Whether you’re a recent graduate, or it’s been a long time since you studied with us, we’re
                    here to help keep you involved with the University and your alumni network.</b> </p>
                <p>No matter where life takes you, we want you to feel part of our community forever and we hope you continue
                    to share the spirit long after your graduation day.</p>
            </div>
        </div>
    </div>
</section>

<section class="services-card-slider">
    <div class="container-fluid">
        <div class="service-slider owl-theme owl-carousel">
            <div class="card-item">
                <div class="card-img">
                    <img class="img-fluid img-deflt" src="img/services/HU-Alumni-card.jpg" alt="">
                    <img class="img-fluid img-respo" src="img/services/HU-Alumni-card-res.jpg" alt="">
                </div>
                <div class="card-content">
                    <h5>Habib University Alumni Card</h5>
                    <p>The Habib University Alumni Card identifies you as a proud Habib University alumnus or aluma and gives you access to a host of services both on and off campus. These benefits range from access to various spaces on campus to partner discounts through collaborations that the University hopes to build in the coming years. You will need your Alumni Card to access campus during alumni visiting hours, which are from noon to 7:30 p.m. Monday through Friday and during working Saturdays (usually the second Saturday of each month).</p>
                    <p>The Alumni Card is awarded on a complimentary basis to all Habib University graduates, and remains in force as long as alumni remain in good standing and have not abused their privileges or violated the University’s Code of Conduct. In the event of the loss or theft of your card, please report it to the Office of Alumni Affairs promptly. Alumni Affairs will be glad to arrange for a replacement card for the actual cost of the card (currently PKR 1,250). If Security or other responsible University authorities request to see your card, please present it to them promptly when requested. Failure to do so may result in the suspension of your access privileges.</p>
                </div>
            </div>
            <div class="card-item">
                <div class="card-img">
                    <img class="img-fluid img-deflt" src="img/services/Access-to-campus.jpg" alt="">
                    <img class="img-fluid img-respo" src="img/services/Access-to-campus-res.jpg" alt="">
                </div>
                <div class="card-content">
                    <h5>Access to Campus</h5>
                    <p>Your Alumni Card will grant you access to all public spaces on campus. We welcome you back to campus for occasional visits when your schedule permits, but we ask alumni to bear in mind that it’s important that current students have priority in enjoying all the benefits of campus. As our campus is limited in its size, each additional user of campus resources puts an added strain on those resources. Therefore, to ensure that current students have the same quality experience on campus that alumni enjoyed as students, we ask that alumni keep the frequency and duration of their visits to campus reasonable. Please keep your visits to open public areas of campus, and avoid entering classrooms or labs without specific invitations from instructors. If you are hoping to visit with a specific faculty member, we ask that you make an appointment with them in advance to ensure that your visit is expected and not inconvenient for them.</p>
                </div>
            </div>
            <div class="card-item">
                <div class="card-img">
                    <img class="img-fluid img-deflt" src="img/services/IT-services.jpg" alt="">
                    <img class="img-fluid img-respo" src="img/services/IT-services-res.jpg" alt="">
                </div>
                <div class="card-content">
                    <h5>IT Services</h5>
                    <ul>
                        <li> <img src="img/services/tick-bullert-poit.svg" class="me-2" alt=""> Alumni email is a complimentary service available to all alumni for free.</li>
                        <li> <img src="img/services/tick-bullert-poit.svg" class="me-2" alt=""> Graduating students’ email accounts will remain intact with the new alumni account and can be accessed through portal.office365.com</li>
                        <li> <img src="img/services/tick-bullert-poit.svg" class="me-2" alt=""> Alumni will have access to a number of commercial software applications through portal.office365.com on a complimentary basis, including:
                            Outlook, OneDrive, Word, Excel, PowerPoint, OneNote, Class Notebook, Sway, Calendar, Forms, Tasks and Video</li>
                        <li> <img src="img/services/tick-bullert-poit.svg" class="me-2" alt=""> IT applications may be added or removed subject to availability.</li>
                        <li> <img src="img/services/tick-bullert-poit.svg" class="me-2" alt=""> Alumni can access the HU IT Service Desk for the resolution of IT concerns.</li>
                        <li> <img src="img/services/tick-bullert-poit.svg" class="me-2" alt=""> The HU IT Department reserves the right to add, change, terminate or suspend any of Alumni IT services at its sole discretion.</li>
                     </ul>
                </div>
            </div>
            <div class="card-item">
                <div class="card-img">
                    <img class="img-fluid img-deflt" src="img/services/library.jpg" alt="">
                    <img class="img-fluid img-respo" src="img/services/library-res.jpg" alt="">
                </div>
                <div class="card-content">
                    <h5>Library</h5>
                    <p>Alumni who choose to access and use the Habib University are welcome to do so if they subscribe to an annual membership fee of Rs. 5,000/- This annual membership, which runs from January 1st – December 31st, will allow you to use the Habib University Library resources, including its different spaces, books, magazines, computers and online resources. Additionally, those alumni wishing to borrow books can do so by paying a fully refundable security deposit of Rs. 10,000/- at the time of registration.</p>
                </div>
            </div>
            <div class="card-item">
                <div class="card-img">
                    <img class="img-fluid img-deflt" src="img/services/Gym-and-pool-facilty.jpg" alt="">
                    <img class="img-fluid img-respo" src="img/services/Gym-and-pool-facilty-res.jpg" alt="">
                </div>
                <div class="card-content">
                    <h5>Gym and Pool Facilities</h5>
                    <p>Alumni who wish to use the gym and pool may also purchase a monthly membership for PKR 2,500/month.</p>
                    <h5 class="sub-h">Gym (Men & Women)</h5>
                    <p>Monday to Friday 5pm to 8pm</p>
                    <h5 class="sub-h">Gym (Men & Women)</h5>
                    <p>Women: Monday , Wednesday & Friday 5pm-7:30pm</p>
                    <p>Men: Tuesday , Thursday & (working) Saturday 5pm-7:30pm</p>
                    <p>Non working Saturday 12pm-6pm</p>
                </div>
            </div>
            <div class="card-item">
                <div class="card-img">
                    <img class="img-fluid img-deflt" src="img/services/Sports-facilities.jpg" alt="">
                    <img class="img-fluid img-respo" src="img/services/Sports-facilities-res.jpg" alt="">
                </div>
                <div class="card-content">
                    <h5>Sports Facilities</h5>
                    <p>In the spirit of sportsmanship, Alumni will be invited to participate in matches, tournaments and other events at the Habib University from time to time. To ensure that current students have access to our basketball and tennis courts, as well as other recreational spaces, we ask that alumni not use those facilities unless they are participating in a scheduled game or tournament for alumni that they have been invited to participate in.</p>
                </div>
            </div>
        </div>
    </div>
</section>


  <?php include 'include/footer.php' ?>