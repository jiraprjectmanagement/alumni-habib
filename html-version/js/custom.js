








// Project Slider
$('.projects-area-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: true,
    margin:25,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
        },                   
        574:{
            items:2,
            dots: true,
            nav: false,
        },
        766: {
            items: 2,  
            dots: true,
            nav: false,
        }, 
        990: {
            items: 3,
            dots: true,
            nav: true,
        }, 
        1199: {
            items: 4,
            dots: true,
            nav: true,
        }
    }
});
// Project Slider




if($(window).width() <= 768){
    if(('.events-box-slider').length != 0){
        $('.events-box-slider').addClass('owl-carousel owl-theme');
        $('.events-box-slider').owlCarousel({
            loop:false,
            margin:15,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                413:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                574:{
                    
                    items:2,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:2,
                }
                
            }
        });
    }
}

if($(window).width() <= 768){
    if(('.featured-box-area').length != 0){
        $('.featured-box-area').addClass('owl-carousel owl-theme');
        $('.featured-box-area').owlCarousel({
            loop:false,
            margin:15,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                480:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                574:{
                    
                    items:2,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:3,
                }
                
            }
        });
    }
}



// Menu Toggle
$('#nav-icon3').click(function(){
    $(this).toggleClass('open');
});
// Menu Toggle

$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $("header").addClass("header-sticky");
        // $("header").removeClass("bg-header");
    }
    else{
        $("header").removeClass("header-sticky");
        // $("header").addClass("bg-header");
    }
   
});

if($(window).width() >= 1199){
$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 500;
    
    if(scrollheader){
        $(".we-proud-area").addClass("we-proud-active");
        // $("header").removeClass("bg-header");
    }
    else{
        $(".we-proud-area").removeClass("we-proud-active");
        // $("header").addClass("bg-header");
    }
   
});
}


// window.onscroll = function() {
//     var theta = document.documentElement.scrollTop / 50 % Math.PI;

// document.getElementById('js-logo').style.transform ='rotate(' + theta + 'rad)';
// document.getElementById('js-logo-one').style.transform ='rotate(' + theta + 'rad)';
// }
AOS.init();
